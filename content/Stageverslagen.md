+++
Categories = ["Stage"]
Tags = []
date = "2016-05-05T13:51:01+02:00"
title = "Stage bij Van Roey Turnhout"

+++
![Image of Van Roey Turnhout](https://www.webstore.be/media/vra-turnhout.jpg)
##### Dag 1
Eerste dag goed begonnen met 2 laptops uit elkaar te halen en er bij de ene een nieuw moederbord ingestoken en een harde schijf en bij de andere een nieuw moederbord en een nieuwe LCD kabel.
<br>
Ook de besturingssystemen herstelt en een backup genomen van de gegevens.
<br>
Er was ook een kleine rondleiding op de afdeling.
<br>
***
##### Dag 2

Eerste paar uren meegekeken met de stagementor bij het opnemen van oproepen en het helpen van klanten op afstand.
<br>
Ook terug een laptop uit elkaar gehaald en een harde schijf vervangen.
<br>
Dan de oude harde schijf extern aangesloten op een andere laptop om hier de gegevens op over te zetten.
<br>
***
##### Dag 3

Op 2 laptops ook office en antivirus geïnstalleerd en ook geupgrade naar windows 10.
<br>
Laptop met opstartproblemen bekeken en geprobeerd hem te maken.
<br>
Dit was niet gelukt zonder hulp maar had wel een backup van de gegevens genomen voor het geval dat.
<br>
De harde schijf van een fysieke computer is ook vervangen.
<br>
***
##### Dag 4

Dag begonnen met het vernietigen van alle overgebleven hardware en dozenn die op ik op datum heb moeten selecteren.
<br>
Daarna een desktop moeten nakijken omdat deze problemen had met de internet verbinding. (Drivers geüpdate, ping test gedaan, enz..)
<br>
Dan met opdracht over de CaseStudy bezig want er was weinig te doen en er werd veel gebeld.erste taak: computer verschonen en helemaal vernieuwen.
<br>
***
##### Dag 5

Begonnen met de overgebleven laptops te sorteren, schermen terug in te stellen en kabels te sorteren.
<br>
Ook een switch geplaatst en hier terug alle ethernet kabels gesorteerd.
<br>
Dan ethernet kabels geordend en wat verplaatst en opgekuist.
<br>
Verder ook nog een herstel uitgevoerd op een moederbord maar dit was weinig werk omdat dit na verder onderzoek toch kapot was.  
<br>
***
##### Dag 6
Problemen met office opgelost en harde schijf op een desktop vervangen.
<br>
Daarna Windows 8 op deze harde schijf gezet en natuurlijk ook geconfigureerd.
<br>
Ook laptop binnengekregen die problemen had met harde schijf maar na enkele tests was gebleken dat de harde schijf gewoon kapot was en is hiervoor een nieuwe besteld.
<br>
Dan ook nog een laptop binnengekregen waarvan de internetverbinding getest is en hieruit was gebleken dat het wifi kaartje kapot was en is er dus ook een nieuw besteld.
<br>
***
##### Dag 7

Begonnen met een gesprek met de directie over enkele gegevens die nodig waren voor bedrijfsbeheer.
<br>
Hierna deze gegevens uitgetypt. Daarna meegekeken met het vervangen van het scherm van een gsm toestel. In de namiddag nog een moederbord vervangen.
<br>
***
##### Dag 8

Vele notebooks uit elkaar gehaald en hier telkens 4GB RAM bijgestoken of vervangen.
<br>
Dan een desktop geconfigureerd met Windows 10 op en hier ook de basisprogramma's en office geïnstalleerd.
<br>
Als laatste nog de ethernet beter aangesloten op een switch om hier dan de testschermen en de pc's met ethernet te laten kunnen verbinden.
<br>
***
##### Dag 9
Dag begonnen met het nakijken van een laptop waar het beeldstuurprogramma niet meer werkt.
<br>
Dit was een klein probleem met de driver en dus was het zo opgelost.
<br>
Daarna aan de GIP gewerkt (frans) en dit ook nog een lange tijd gedaan.
<br>
Dan in de namiddag een gaming pc (die zelf samengesteld was) nagekeken omdat deze hele tijd vast hing en er mogelijk niet genoeg schijfruimte was.
<br>
De drivers zijn geüpdatet en momenteel zijn er geen andere problemen mee.
<br>
***
##### Dag 10

Laatste dag was het heel rustig. Enkel een paar backups moeten nemen.
<br>
Daarna aan GIP gewerkt en alles opgeruimd.
<br>
