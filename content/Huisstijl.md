+++
Categories = ["Software"]
Tags = []
date = "2016-03-05T13:51:01+02:00"
title = "Huisstijl stagebedrijf Van Roey"

+++
![Image of Van Roey](../images/VanRoey.png)
<br>
Van Roey heeft allereerst zoals de meeste bedrijven een logo.
<br>
Dit logo wordt gebruikt voor zowel de business kant als bij de kant van de consumenten.
<br>
Zowel op de website als op de webstore wordt het logo getoond.
<br>
In de winkel zelf heb je minder te maken met het logo maar meer met andere typische aspecten.
<br>
Als je bijvoorbeeld een herstelde computer terugkrijgt zal hier een typische blauwe factuur aan vast hangen met hierop alle informatie.
<br>
Ze hebben zowel blauwe (klantenfacturen) als roze(leveranciersfacturen) die de administratie voor hun veel handiger maakt.
<br>
Als je met een probleem of vraag zou zitten en er een email wordt teruggekregen van één van de collega’s
<br>
dan is deze automatisch uitgerust met de naam van de medewerker, zijn functie en de gegevens van Van Roey.
<br>
Ook de gekke reclame van Van Roey kan je onder huisstijl omvatten. Met de grappige boodschappen en de subtiele reclame kan je Van Roey ook herkennen van andere.
