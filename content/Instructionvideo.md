+++
Categories = ["IT-tapa"]
Tags = []
date = "2015-12-01T13:51:01+02:00"
title = "Instruction guide"

+++

##### Instruction video about android
We made an instruction video to show you how to add an account to your smartphone.
<br>
If you'd like to check it out, just click on the video below!

[![InstructionVideo](http://wout-wynen.github.io//images/Englishvideo.png)](https://www.youtube.com/watch?v=liHLTloaZ1E "English video tutorial")
