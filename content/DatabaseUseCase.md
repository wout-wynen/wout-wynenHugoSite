+++
Categories = ["Software"]
Tags = []
date = "2016-03-04T13:51:01+02:00"
title = "Wikipedia database"

+++

 Mijn UseCase diagramma is gebaseerd op het concept van wikipedia.
 <br>
 De SQL code die gebaseerd is op dit UseCaseDiagramma kan u [hier](https://gitlab.com/wout-wynen/WikipediaSQL) terug vinden.
 <br>
![Image of UseCase](../images/UseCase.png)  
