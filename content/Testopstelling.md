+++
Categories = ["Systeembeheer"]
Tags = []
date = "2016-05-10T13:51:01+02:00"
title = "Testopstelling: PHP met MYSQL (voorlopige versie)"

+++

#### Korte samenvatting van onze testopstelling en testen.
#### PHP: (mario)
Bij het creëren van de testopstelling hebben we vooral met Linux Xubuntu gewerkt.
<br>
Een mogelijk alternatief was de Windows server met IIS, maar hier doken enkele problemen op in verband met het maken van de connectie tussen de database en de webpagina.
<br>
Om de webpagina lokaal te kunnen testen op de Xubuntu hebben we gebruik gemaakt van apache2.
<br>
Handig hieraan is dat het samen met de Virtuele machine word opgestart en dat je bestanden altijd gehost worden.
<br>
De bestanden die gehost worden plaats je in de var/www/html map van het Device “Computer”.
<br>
Om in deze map te geraken moet je wel een programma installeren via Sudo apt-get install Nautilus.
<br>
Eenmaal je bestander hier gezet kan je ze ook nog steeds aanpassen.  
![Image of Testopstelling1](../images/Testopstelling1.png)
<br>
Php is verder een zeer handige programmeertaal aangezien het de mogelijkheid bied de verbinding met mysql op eenvoudige manier te maken.
<br>
Echter moet je je ervan bewust zijn dat je de queries goed laat aankomen en dat ze “hackproof” zijn tegen sql injection attacks.

#### MYSQL (Wout)
Als testopstelling hebben we ook het opensource-managementsysteem voor databases namelijk MYSQL gebruikt.
<br>
Omdat we willen testen dat wanneer je een klasnummer ingeeft de gebruikt een leerling terugkrijgt , moeten we met een database werken.
<br>
We hebben in MYSQL Workbench een database ‘testopstelling’ met een tabel ‘klagenoten’ gemaakt met hierin verschillende query's.
<br>
Het werken met MYSQL is in het algemeen goed verlopen. We hebben kleine problemen gehad met het installeren van PhpMyAdmin was moeilijk verlopen.
<br>
Nadat dit was gelukt hebben we geen enkel probleem meer gehad.
<br>
De code in MYSQL zelf was ook niet moeilijk omdat we dit met het vak software ook al veel met SQL bezig geweest zijn.
![Image of Testopstelling2](../images/Testopstelling2.png)
<br>
Je kan zelfs een code genereren zodat je zelf de code niet moet schrijven.
<br>
Dit heeft wel een nadeel omdat je wanneer je een fout hebt of een query moet toevoegen, je geen idee hebt hoe dit werkt en dus ook een nieuwe tabel zal moeten maken.
#### Linken van PHP met MYSQL (Greg)
Php en  mysql kan werken met een mysql database door het gebruik van MySQL extension of een PDO (PHP data objects).
<br>
Zowel MySQL en PDO hebben hun voordelen. MySQLi zal alleen werken met SQL databases en PDO kan met 12 verschillende databases werken.
<br>
Als je moet switchen tussen verschillende databases is het het best om dus PDO te gebruiken aangezien de verschillende databases die je kan gebruiken.
<br>
Met MySQLi moet je de hele code herschrijven wat een voordeel is en ook een nadeel.
<br>
Het voordeel is dat je zelf de specificaties kan schrijven het nadeel is dit vraag veel kennis werk en moeite.
<br>
Beiden zijn object georiënteerd maar MySQLi bied een API aan.
<br>
![Image of Testopstelling3](../images/Testopstelling3.png)
