+++
Categories = ["Frans"]
Tags = []
date = "2015-10-21T13:51:01+02:00"
title = "Foire Aux Questions"

+++
#### J'espère que cette FAQ vous aidera un peu avec votre problème
<br>
![Image of FAQ](http://www.remindmecare.com/wp-content/uploads/2015/08/FAQ.jpg)

##### Question 1:  Est-il possible d’ajouter un compte Facebook de cette manière?
Réponse 1: Oui, c’est possible. Cela fonctionne de la mëme manière.

##### Question 2: Vous pouvez lier votre courrier électronique à l’appli de Smartschool?
Réponse 2: Non, c’est ne pas possible sur le smartphone, seulement sur votre ordinateur.

##### Question 3: Vous pouvez lier plusieurs comptes à Google?
Réponse 3: Oui , c’est possible. Vous pouvez ajouter aussi un compte privé par exemple.
Vous pouvez changer toujours entre les comptes.


##### Question 4: Où pouvez-vous régler quelles applications peuvent se synchroniser?
Réponse 4: Vous pouvez régler les paramètres et puis les comptes.
Là-bas vous pouvez choisir “Google”. Ensuite, vous choisissez un compte.
Finalement vous pouvez choisir quelles applications peuvent se synchroniser.


##### Question 5: Quelle est l’utilité la de synchronisation des comptes ?
Réponse 5: Il est utile pour beaucoup de choses.
Vous pouvez gérer des rendez-vous et des documents plus faciles et quand vous avez un rendez-vous par example, vous obtenez un message sur le smartphone.

##### Question 6:  Est-ce qu’il y a beaucoup de risques quand j’ai lié mon compte à mon smartphone?
Reponse 6: Non, pas du tout. Il n'y a aucun risque quand vous avez votre smartphone avec vous.Mais le moment que vous perdez votre smartphone, tout le monde peut à votre email, ordre du jour, etc.. sans un mot de passe.
