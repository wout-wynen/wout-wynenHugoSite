+++
Categories = ["Engels"]
Tags = []
date = "2016-05-04T13:51:01+02:00"
title = "Review of a software tool in the training company: Van Roey Automation"

+++


##### N-able software review (solarwinds company)
![Image of SolarwindsLogo](http://i.forbesimg.com/media/lists/companies/solarwinds_416x416.jpg)
<br>
The N-able software is from the company Solarwinds and is one of the global leaders in IT management software and IT service providers.
<br>
It is specialized in providing a lot of information about the devices connected in a network.
<br>
Many companies use it to ease the communication between departments and help out the customers as quickly as possible.
<br>
With the advanced techniques it is also able to notify the users when a technical difficulty is happening with any of the devices.
<br>
Van Roey uses this program with basically everything. When they arive and put the computer on, the program is already running. They use every feature of the software: communication, service, reports, etc..
<br>
The N-able software is in my opinion great software to use in big companies.
<br>
It is easy to use and it has a lot of monitoring tools.
<br>
I’m not a fan of the monitoring tools, because they are a bit sketchy and hard to understand.
<br>
I think they need to make fewer tools and put more effort into them individually.
<br>
The notification that the staff gets on their phones and computers is a very useful feature.
<br>
There is still one downside to the notifications though:
<br>
When you have installed the app on your phone, there is no way for you to turn the notifications offline without uninstalling the application.


<br>                                                      	        	                    	        	
I don’t know the company personally but based on previous reviews and the opinions of a couple of users, there are not many updates within the company.
<br>
This means that there are sometimes flaws and bugs in the system.
<br>
I’ve heard that the company have been running pretty flat for more than 5 years now and that doesn’t surprise me. I haven’t heard of this company even once and if I hadn’t done my internship with a company that uses this software, I would probably never have heard of it.
<br>
Although they are one of the global leaders in IT management software, I still think that there is a lot of room for improvement when it comes to publicity and updates.
<br>
**The N-able software is great in general, but it has its flaws.**
<br>
I don’t know the price of the software. There is no information about it on the site and I can’t seem to find an estimation anywhere. However, If it is not too expensive, I’d definitely recommend it but right now it does not earn my vote.
