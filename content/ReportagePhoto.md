+++
Categories = ["Stage"]
Tags = []
date = "2016-03-04T13:51:01+02:00"
title = "Un reportage de photo concernant le stage"

+++
**Pour mon reportage de photo j'ai utilisé Prezi  pour présenter mes photos. Cliquez ici pour commencer la présentation:**
<br>

<iframe id="iframe_container" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen="" width="700" height="500" src="https://prezi.com/embed/e06uhhxazsi7/?bgcolor=ffffff&amp;lock_to_path=1&amp;autoplay=0&amp;autohide_ctrls=0&amp;landing_data=bHVZZmNaNDBIWnNjdEVENDRhZDFNZGNIUE43MHdLNWpsdFJLb2ZHanI5Nkt6aXlwTGI4ZkE0clRkUFRKR0w0b2xRPT0&amp;landing_sign=lC2qcrJKMDAHVJskIVAAvQvVaq1BGpAqQrTHdo5o8K4"></iframe>
