+++
Categories = ["Engels"]
Tags = []
date = "2015-10-21T13:51:01+02:00"
title = "MY LIFE AND IT"

+++

<br>
I am Wout Wynen, a 17-year old student in the 6th year at the Immaculata High school in Oostmalle and I’m going to tell you a little bit more about my life in IT so far. What I have accomplished, what I want to accomplish ,what I love about IT management and so on.
<br>
My first experience with IT was back when I was just a little boy of about 6 years old. My parents bought their first computer and I was astonished about what it was and how it worked. At first I didn’t know how the concept of internet worked but after spending many hours learning, I finally managed to play my first game. I found it to be so much fun that I spent some days after school just playing the game and discovering new things. I still don’t really know how I managed to play it without any help but I’m kind of proud of it now.

Since I was into computers at such a young age, I’ve been interested in computers ever since I discovered them. IT means a lot to me now but in a totally different way than before.
I like the different aspects of computer programming and hardware more than ever before. The gaming aspect has become less interesting for me over the past year because I simply prefer discovering and learning new things for example: how to build a site. I spend a lot of my free time on the internet but not in an addictive way. I spend mostly about two hours on a regular day but that’s schoolwork included. Sometimes I feel like doing some programming but on other days I just prefer to play games with friends. I would not consider myself an expert but maybe in the future that title will come my way.


I’d like to do something with IT for a living. I’m not really sure what field of IT I am more interested in. Hardware and software are both very interesting fields for me but I think after this year, I will have my answer. At the moment I like the software part a bit more but that might just be for a short period of time because last year I was more into hardware.

My life would be totally different without IT. I have no idea what I would be doing then, and I don’t want to think about it anyway. I like the way my life is right now and I am doing what I love doing the most. When you study IT, it’s of course a major part in your life but not in a bad way. You can talk with friends even after school, play games with them, and just have fun in general.  
I hope I will find a job in IT someday because then I never have to work a day of my life.
<br>

![Image of Wout Wynen](https://scontent-ams2-1.xx.fbcdn.net/hphotos-xpt1/t31.0-8/12138391_914998781870469_5782153303557822632_o.jpg)
