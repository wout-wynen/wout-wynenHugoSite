+++
Categories = ["IT-tapa"]
Tags = []
date = "2015-11-15T13:51:01+02:00"
title = "Android presentatie - middagsessie"

+++

Onze  [IT-tapa](https://www.facebook.com/IT-tapa-588405757863108/) sessie ging over het gebruik, de handige functies en apps van [Android](https://www.android.com/).
Samen met mijn duo partner Yorick hebben we de leerkracht en leerlingen toch wat bijgeleerd over:

 - - Accounts toevoegen op Google
 - - Sneller en handiger werken met Android
 - - Nuttige apps en tips voor smartphones
 - - Beveiliging op Android

 Na het geven van de IT-tapa sessie was ik zelf toch aangenaam verrast. <br>
 Ik vond het een heel leuke en leerrijke ervaring om de rollen is te mogen omdraaien en deze keer zelf eens 'les' te geven.
 Het was helemaal anders dan een gewone presentatie of een voorstelling.
 Het meehelpen van de leerkrachten en leerlingen was toch nog leuker als anders en ik denk dat mijn partner dit ook vond.
 <br>
 De website van Yorick vind je [hier](http://www.disite.be/yorick.scheyltjens/index.php).

![Image of Wout Wynen](http://wout-wynen.github.io//images/IT-tapa1.png)
![Image of Wout Wynen](http://wout-wynen.github.io//images/IT-tapa2.png)
![Image of Wout Wynen](http://wout-wynen.github.io//images/IT-tapa3.png)
