+++
Categories = ["Stage"]
Tags = []
date = "2016-03-04T13:51:01+02:00"
title = "CaseStudy : N-able software"

+++

#### Wat?
![Image of N-Central](../images/N-central.png)

**Als onderwerp van de CaseStudy heb ik de software n-able gekozen die mijn stagebedrijf Van Roey gebruikt.**
<br>
Deze software is bedoeld om de gebruikers en de klanten hulp te bieden.
<br>
De software houdt zich vooral bezig met het vergemakkelijken van alledaagse taken die door het personeel gedaan moeten worden.
<br>
Als men bijvoorbeeld s’morgens op het werk aankomt, is het de bedoeling dat alle computers van klanten en personeel een systeemcheck krijgen.
<br>
Dit wordt automatisch gedaan door de N-able software.
<br>
Het personeel moet bij het opstarten van de computers ook inloggen op het programma om zo een overzicht te krijgen van welke collega’s er niet op het werk zijn of eventueel te laat zijn door file.

#### Op stagebedrijf

![Image of N-Central](../images/N-central2.png)
<br>
Wanneer klanten hardware binnenbrengen, wordt dit automatisch verbonden met de N-able software.
<br>
Deze sofware zorgt voor de beveiliging in verband met virussen en spyware.
<br>
Ook wordt er automatisch een backup genomen van de harde schijf om te voorkomen dat de klant zijn gegevens verloren gaan.
<br>
Wanneer het personeel reparaties wil uitvoeren aan een computer van een klant, moet dit eerst op het programma geregistreerd worden, zodat iedereen hiervan op de hoogte is.
<br>
Het krijgen van notificaties op je mobiele apparaten is ook een heel handige functie aan de software.
<br>
Het personeel moet een applicatie downloaden dat gelinkt is aan het programma.
<br>
Hierdoor krijgen ze telkens meldingen op hun apparaat wanneer er bv. iets mis is met de computer, dat de netwerkverbinding niet optimaal is, enz...
<br>
Dan is er nog een laatste maar zeker niet te vergeten functie van de tool. Als er klanten met een informatica probleem zitten, bellen ze meestal naar de informatica afdeling bij Van Roey. Dit gebeurt ook via de N-able software.
<br>
Het personeel krijgt een melding op zijn computer dat er een klant belt. Hiermee kunnen ze dan ook opnemen of de klanten doorverbinden met de juiste persoon.
<br>
Van Roey zelf werkt er nog niet lang mee, maar ze zijn nu al heel tevreden over de prestaties die het programma heeft geleverd. Het bespaart de firma veel tijd en kosten.
<br>

##### Eigen mening over de software
![Image of opinion](http://www.clipartbest.com/cliparts/dc6/Raa/dc6RaaKc9.jpeg)

Voor de korte tijd dat ik ermee gewerkt heb en deze software echt gebruikt heb, vind ik het zeker een aanrader voor grote bedrijven.
<br>
Het spaart veel tijd uit en zorgt ervoor dat iedereen optimaal samenwerkt met elkaar.
<br>
Een groot nadeel is wel dat er nergens vermeld staat hoeveel deze software kost, en dus je ook niet kan zien op voorhand of dit de moeite wel is.
<br>
Zeker geen aanrader voor mensen die zelfstandig alleen werken of een kleine onderneming zijn.
<br>
Verder niets slecht op te zeggen. Deze software kan wel eens heel bekend worden als grote bedrijven dit gaan gebruiken.
