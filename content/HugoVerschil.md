+++
Categories = ["Software"]
Tags = []
date = "2015-10-29T13:51:01+02:00"
title = "WORKFLOWS"

+++
#### Wat is Hugo?
Hugo is een static website engine.

 - Het kan efficient worden gebruikt en is met wat kennis vrij snel opgebouwd.
 - De posts worden in Markdown geschreven en de rest van de site vooral in HTML en CSS.
#### Positieve en negatieve kanten

##### Wat ik goed vond aan Hugo:

 - -Posts zijn makkelijk te maken en in markdown geschreven.

 - -Veel verschillende thema's die uitstekend gemaakt zijn.

 - -Makkelijk te gebruiken met github (pages) en andere hosting sites.

##### Wat ik minder goed vond aan Hugo:

 - -Om grondige aanpassingen te doen moet je wel wat kennis hebben van HTML en CSS.

 - -Thema's zijn soms moeilijk te begrijpen.

 - -Geen hulp, veel zelfstandig uitzoeken.

#### Mening over Hugo

Hugo is een interessante engine als je blog wil opbouwen. Zelf weet ik niet of ik ook dezelfde zou gekozen hebben maar dit is vooral omdat ik niet echt andere static website engines ken.
Als je je even in Hugo verdiept kan je vrij veel bijleren en ook wel plezier in het maken van je site ondervinden als je het test.
Ik vind het niet zo gemakkelijk om er iets in aan te passen maar dit zorgt er wel voor dat het heel belonend is als je wel iets kan aanpassen waar je al een tijdje achter zat te zoeken.
Hierin kan je gewoon met een interface een post toevoegen en hoef je je geen zorgen te maken over bepaalde dingen zoals opmaak.
Wordpress is dus ook makkelijker te gebruiken dan Hugo maar dit wil ook zeggen dat je dan niet zo uitgebreid je site kan aanpassen.

#### Structuur van Hugo
De structuur van Hugo vind ik persoonlijk soms ingewikkeld maar na veel gebruiken werd dit duidelijker voor mij.
Als je een thema gebruikt en dit dan ook kloont van Gitlab, staat dit in een aparte map bij themes maar dit is makkelijk over te zetten naar de standaard mapjes van Hugo.
Dan heb je ook nog een mapje content waar al je posts staan.
Ook de map archetypes en de mappen layouts en static zijn erg belangrijk voor je website.
In de map layouts vind je je default thema waarop je site is gebaseerd. Dit is aanpasbaar maar ik heb dit uitgetest en vind dit nogal moeilijk om te doen.
Dan in de map static staan je afbeeldingen en hier vind je ook al de Javascript en CSS bestanden. Hier heb ik niets aan veranderd.
Dan als laatste heb je je public map. Hier word eigenlijk je website in gegenereerd om deze online te plaatsen.
Wat ik wel heb gemerkt aan de map is dat hier vaak fouten in kunnen komen als je bv overschakelt van twee thema's, daarom is dit ook vrij belangrijk om deze te wissen en om een nieuwe public map te laten genereren.





#### Conclusie

 - Hugo is een goeie keuze als je veel wilt bijleren over hoe je een blog moet maken en er graag wel wat tijd aan besteed om het aan te passen.
 - De thema's zijn leuk maar soms ook wel moeilijk om volledig te begrijpen.
 - Perfect voor onze GIP opdracht omdat je er veel in moet gaan zoeken en zelfstandig moet uittesten.
